<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar () {
        return view('form');
      }
  
      public function sesi (Request $sesi) {
        $fname = $sesi->fname;
        $lname = $sesi->lname;
        $gender = $sesi->gender;
        $nationality = $sesi->nationality;
        $language = $sesi->language;
        $language .= ' '.$sesi->language2;
        $language .= ' '.$sesi->language3;
        $bio = $sesi->bio;
        return view('welcome')->with('fname', $fname)
        ->with('lname', $lname)
        ->with('gender', $gender)
        ->with('nationality', $nationality)
        ->with('language', $language)
        ->with('bio', $bio) ;
      }
  
}
