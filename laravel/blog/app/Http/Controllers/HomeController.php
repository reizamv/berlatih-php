<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index() {
        return view('index');
    }
    public function index1() {
        return view('index1');
    }

    public function data() {
        return view('datatables');
    }

}
