<html>
  <head></head>
  <body>
    <h1>Buat Akun Baru</h1>
    <h2>Sign-Up Form</h2>
    <form class="" action="/daftar/sesi" method="post">
      @csrf
      <p><b>First Name:</b></p>
        <input type="text" name="fname">
      <p><b>Last Name:</b></p>
        <input type="text" name="lname">
      <p><b>Gender:</b></p>
        <input type="radio" name="gender" value="male">Male<br>
        <input type="radio" name="gender" value="female">Female<br>
        <input type="radio" name="gender" value="other">Other<br>
      <p><b>Nationality:</b></p>
        <select class="" name="nationality">
          <option value="indonesian">Indonesian</option>
          <option value="malaysian">Malaysian</option>
          <option value="other">Other</option>
        </select>
      <p><b>Language Spoken:</b></p>
        <input type="checkbox" name="language" value="bahasa">Bahasa Indonesia<br>
        <input type="checkbox" name="language2" value="english">English<br>
        <input type="checkbox" name="language3" value="other">Other<br>
      <p><b>Bio:</b></p>
        <textarea name="bio" rows="5" cols="30"></textarea><br><br>
      <input type="submit" name="" value="Sign-Up">
    </form>
  </body>
</html>
