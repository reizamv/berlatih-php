<?php
function tentukan_nilai($number)
{
    $a="";
    if ($number>=85) {
        $a="<br> <br>{$number} = sangat baik <br>";
    }else if($number>=70 && $number<85) {
        $a="{$number} = baik <br>";
    }else if($number>=60 && $number<70) {
        $a=" {$number} = cukup <br>";
    }else {
        $a="{$number} = Kurang <br>";
    }
echo $a;
}

//TEST CASES
echo tentukan_nilai(98); //Sangat Baik
echo tentukan_nilai(76); //Baik
echo tentukan_nilai(67); //Cukup
echo tentukan_nilai(43); //Kurang
?>